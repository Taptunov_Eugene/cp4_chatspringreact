package com.deveducation.chat.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Game {

    private String whom;

    private String from;

    private Command command;

    public String[] gameState = new String[9];

    private String winner;

    private String turn;
}
