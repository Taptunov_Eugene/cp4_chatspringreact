package com.deveducation.chat.controller;

import com.deveducation.chat.dto.Command;
import com.deveducation.chat.dto.Game;
import com.deveducation.chat.entity.CustomUser;
import com.deveducation.chat.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import java.util.Arrays;

@Controller
public class GameController {
    @Autowired
    UserService userService;

    @MessageMapping("/game")
    @SendTo("/chat/gamer")
    public Game getMessages(@Payload Game game) {

        switch (game.getCommand()) {
            case START:
                return game;
            case JOIN:
                return game;
            case TURN:
                if (check(game)){
                    game.setCommand(Command.WIN);
                    game.setWinner(game.getFrom());
                    return game;
                } else {
                    if (Arrays.stream(game.getGameState()).allMatch(element-> element==null)) {
                        game.setCommand(Command.DRAW);
                        return game;
                    }
                    else {
                        return game;
                    }
                }
        }
        return game;
    }

    public boolean check(Game game) {
        if ((game.gameState[0].equals("1") && game.gameState[1].equals("1") && game.gameState[2].equals("1")) ||
                (game.gameState[0].equals("0") && game.gameState[1].equals("0") && game.gameState[2].equals("0"))) {
            return true;
        } else if ((game.gameState[3].equals("1") && game.gameState[4].equals("1") && game.gameState[5].equals("1")) ||
                (game.gameState[3].equals("0") && game.gameState[4].equals("0") && game.gameState[5].equals("0"))) {
            return true;
        } else if ((game.gameState[6].equals("1") && game.gameState[7].equals("1") && game.gameState[8].equals("1")) ||
                (game.gameState[6].equals("0") && game.gameState[7].equals("0") && game.gameState[8].equals("0"))) {
            return true;

        } else if ((game.gameState[0].equals("1") && game.gameState[3].equals("1") && game.gameState[6].equals("1")) ||
                (game.gameState[0].equals("0") && game.gameState[3].equals("0") && game.gameState[6].equals("0"))) {
            return true;
        } else if ((game.gameState[1].equals("1") && game.gameState[4].equals("1") && game.gameState[7].equals("1")) ||
                (game.gameState[1].equals("0") && game.gameState[4].equals("0") && game.gameState[7].equals("0"))) {
            return true;
        } else if ((game.gameState[2].equals("1") && game.gameState[5].equals("1") && game.gameState[8].equals("1")) ||
                (game.gameState[2].equals("0") && game.gameState[5].equals("0") && game.gameState[8].equals("0"))) {
            return true;

        } else if ((game.gameState[0].equals("1") && game.gameState[4].equals("1") && game.gameState[8].equals("1")) ||
                (game.gameState[0].equals("0") && game.gameState[4].equals("0") && game.gameState[8].equals("0"))) {
            return true;
        } else if ((game.gameState[2].equals("1") && game.gameState[4].equals("1") && game.gameState[6].equals("1")) ||
                (game.gameState[2].equals("0") && game.gameState[4].equals("0") && game.gameState[6].equals("0"))) {
            return true;
        }
        return false;
    }
}
